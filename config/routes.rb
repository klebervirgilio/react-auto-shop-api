Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api, defautls: { format: :json } do
    namespace :v1 do
      post '/tokens', to: 'tokens#create'
      post '/registrations', to: 'registrations#create'

      # FIXME: use token user to handle resources

      resources :repairs do
        resources :comments
      end

      resources :users do
        get :clients, on: :collection
        resources :repairs
      end
    end
  end
end
