class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :email
      t.string :password_digest
      t.boolean :manager
      t.string :full_name
      t.integer :repairs_count, default: 0, null: false

      t.timestamps
    end
    add_index :users, :email, unique: true
  end
end
