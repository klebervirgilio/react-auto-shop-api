class CreateRepairs < ActiveRecord::Migration[5.0]
  def change
    create_table :repairs do |t|
      t.references :user, foreign_key: true
      t.integer :status, default: 0
      t.datetime :scheduled_at

      t.timestamps
    end
    add_index :repairs, :status
  end
end
