class AddManagerIdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :repairs, :manager_id, :integer
    add_index :repairs, :manager_id
  end
end
