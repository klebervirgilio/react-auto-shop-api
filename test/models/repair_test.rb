require 'test_helper'

class RepairTest < ActiveSupport::TestCase
  test "schedule overlaps validation fails" do
    invalid_repair  = Repair.new(scheduled_at: 30.minutes.from_now, user: users(:john))

    assert invalid_repair.invalid?
    assert invalid_repair.errors.messages[:scheduled_at]
  end

  test "schedule overlaps validates pass" do
    valid_repair  = Repair.new(scheduled_at: 2.hours.from_now, user: users(:john))
    assert valid_repair.valid?
  end
end
