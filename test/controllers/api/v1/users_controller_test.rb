require 'test_helper'

class Api::V1::UsersControllerControllerTest < ActionDispatch::IntegrationTest
  test "creates an user" do
    post '/api/v1/users',
    params: {user: {
      email: 'user@example.com',
      password: 'secret',
      password_confirmation: 'secret',
      full_name: 'User Name'
    }},
    headers: {
      'HTTP_AUTHORIZATION' => ActionController::HttpAuthentication::Token.encode_credentials(JsonWebToken.encode({user_id: users(:john).id}))
    }

    assert_response 201
  end

  test "updates an user" do
    john = users(:john)
    john.password = john.password_confirmation = 'secret'
    john.save
    patch "/api/v1/users/#{john.id}",
    params: {user: {full_name: 'Jane'} },
    headers: {
      'HTTP_AUTHORIZATION' => ActionController::HttpAuthentication::Token.encode_credentials(JsonWebToken.encode({user_id: users(:john).id}))
    }

    assert_equal john.reload.full_name, 'Jane'
  end

  test "destroys an users" do
    john = users(:john)
    delete "/api/v1/users/#{john.id}",
    headers: {
      'HTTP_AUTHORIZATION' => ActionController::HttpAuthentication::Token.encode_credentials(JsonWebToken.encode({user_id: users(:john).id}))
    }
    assert_not User.exists?(id: john.id)
  end
end
