require 'test_helper'

class Api::V1::RegistrationsControllerTest < ActionDispatch::IntegrationTest
  test "creates an user" do
    post '/api/v1/registrations', params: {user: {
      email: 'user@example.com',
      password: 'secret',
      password_confirmation: 'secret',
      full_name: 'User Name'
    }}

    assert_response 200
  end
end
