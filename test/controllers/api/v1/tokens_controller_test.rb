require 'test_helper'

class Api::V1::TokensControllerTest < ActionDispatch::IntegrationTest
  test "successfully creates a token" do
    john = users(:john)
    john.password = john.password_confirmation = 'secret'
    john.save

    post '/api/v1/tokens', params: {email: john.email, password: john.password }
    assert @response.body['auth_token']
  end

  test "does not creates a token" do
    post '/api/v1/tokens', params: {email: "email", password: "password" }
    assert_not @response.body['auth_token']
    assert @response.body['errors']
    assert_response 401
  end
end
