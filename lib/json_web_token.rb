module JsonWebToken
  extend self

  def encode(payload)
    JWT.encode({data: payload, exp: 2.weeks.from_now.to_i}, 'secret', 'HS256')
  end

  def decode(token)
    return HashWithIndifferentAccess.new(JWT.decode(token, 'secret', 'HS256')[0])
  end
end
