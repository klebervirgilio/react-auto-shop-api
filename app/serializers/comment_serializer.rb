class CommentSerializer < ActiveModel::Serializer
  attributes :id, :comment, :author, :created_at

  def author
    object.user.full_name
  end

  def created_at
    object.created_at.strftime('%d/%m/%Y')
  end
end
