class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :manager, :full_name, :password, :password_confirmation, :created_at, :visible

  def password; ""; end
  def password_confirmation; ""; end
  def created_at
    object.created_at.to_i
  end

  def visible
    true
  end
end
