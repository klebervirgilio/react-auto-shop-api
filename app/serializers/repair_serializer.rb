class RepairSerializer < ActiveModel::Serializer
  attributes  :user, :manager, :scheduled_at, :visible, :status,
              :status_label, :completed, :id, :user_id, :manager_id,
              :schedule_date, :schedule_time

  has_many :comments

  def visible
    true
  end

  def status_label
    object.complete? ? 'Completed' : 'Incomplete'
  end

  def completed
    object.complete?
  end

  def scheduled_at
    object.scheduled_at.strftime('%d %b %Y %I:%M %P')
  end

  def schedule_date
    object.scheduled_at.strftime('%Y-%m-%d')
  end

  def schedule_time
    object.scheduled_at.strftime('%H:%M')
  end

  def user
    object.user.full_name
  end

  def manager
    object.manager.full_name
  end
end
