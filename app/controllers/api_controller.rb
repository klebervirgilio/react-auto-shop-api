class ApiController < ApplicationController
  include ActionController::Serialization
  include ActionController::HttpAuthentication::Token

  def current_user
    load_user
  end

  protected

  def load_user
    @current_user ||= begin
      token, _ = token_and_options(request)
      User.find_by(id: JsonWebToken.decode(token)[:data][:user_id])
    end
  end

  def user_signed_in?
    !!load_user
  end

  def require_authentication
    render json: {message: 'Authentication required'}, status: :unauthorized unless user_signed_in?
  end

  def repair_scope
    if current_user.manager?
      Repair
    else
      current_user.repairs
    end
  end
end
