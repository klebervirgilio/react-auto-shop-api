class Api::V1::RegistrationsController < ApplicationController
  def create
    user = User.new(user_params)

    if user.save
      render json: { user: user }
    else
      render json: { errors: user.errors.messages }, status: :unprocessable_entity
    end
  end


  def user_params
    params.require(:user).permit(:email, :full_name, :manager, :password, :password_confirmation)
  end
end
