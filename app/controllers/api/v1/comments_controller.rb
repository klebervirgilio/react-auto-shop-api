class Api::V1::CommentsController < ApiController
  before_action :require_authentication

  def index
    repair = repair_scope.find(params[:repair_id])
    render json: repair.comments.recent
  end

  def create
    repair = repair_scope.find(params[:repair_id])
    comment = repair.comments.create({user_id: current_user.id}.merge(params.permit(:comment)))
    render json: comment
  end
end
