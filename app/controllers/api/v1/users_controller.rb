class Api::V1::UsersController < ApiController
  before_action :require_authentication

  def index
    render json: User.all
  end

  def clients
    render json: User.not_manager.as_json(only: [:id, :full_name])
  end

  def destroy
    User.find(params[:id]).destroy
    head :no_content
  end

  def show
    render json: User.find(params[:id])
  end

  def update
    user = User.find(params[:id])

    if user.update(user_params)
      render json: { user: UserSerializer.new(user) }
    else
      render json: { errors: user.errors.messages }, status: :unprocessable_entity
    end
  end

  def create
    user = User.new(user_params)

    if user.save
      render json: { user: user }, status: :created
    else
      render json: { errors: user.errors.messages }, status: :unprocessable_entity
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :full_name, :manager, :password, :password_confirmation)
  end
end
