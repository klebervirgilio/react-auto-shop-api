class Api::V1::RepairsController < ApiController
  def index

    repairs = if current_user.manager? && params[:user_id].present?
      repair_scope.where(user_id: params[:user_id])
    else
      repair_scope.all
    end

    render json: repairs
  end

  def show
    render json: repair_scope.find(params[:id])
  end

  def destroy
    repair_scope.find(params[:id]).destroy
    head :no_content
  end

  def update
    repair = repair_scope.find(params[:id])

    if repair.update(repair_params)
      render json: { repair: RepairSerializer.new(repair) }
    else
      render json: { errors: repair.errors.messages }, status: :unprocessable_entity
    end
  end

  def create
    repair         = Repair.new(repair_params)
    repair.manager = current_user

    if repair.save
      render json: { repair: repair }, status: :created
    else
      render json: { errors: repair.errors.messages }, status: :unprocessable_entity
    end
  end

  private

  def repair_params
    params.
      require(:repair).
      permit(:scheduled_at, :status, :user_id, :manager_id, :schedule_date, :schedule_time).tap { |hsh|
        schedule_date, schedule_time = hsh.delete(:schedule_date), hsh.delete(:schedule_time)
        if schedule_date && schedule_time
          hsh[:scheduled_at] = [schedule_date, schedule_time].join(' ')
        end
      }
  end
end
