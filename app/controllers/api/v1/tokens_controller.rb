class Api::V1::TokensController < ApplicationController
  include ActionController::Serialization

  def create
    user = User.find_by(email: params[:email])

    if user && user.authenticate(params[:password])
      render json: {
        auth_token: JsonWebToken.encode({user_id: user.id}),
        user: UserSerializer.new(user)
      }
    else
      render json: {errors: {login: "Invalid Credentials"}}, status: :unauthorized
    end
  end
end
