class User < ApplicationRecord
  has_secure_password

  has_many :repairs

  scope :not_manager, -> { where.not(manager: true) }

  validates :email, email_format: true, presence: true, uniqueness: true
  validates :full_name, presence: true
end
