class Repair < ApplicationRecord

  has_many :comments
  belongs_to :user, counter_cache: true
  belongs_to :manager, class_name: 'User', foreign_key: :manager_id

  enum status: [:incomplete, :complete]
  validate :validate_overlaping_schedules

  private
  def validate_overlaping_schedules
    return if persisted?
    if Repair.exists?(scheduled_at: scheduled_at..scheduled_at + 1.hour)
      errors.add(:scheduled_at, "overlaps with other(s) schedules")
    end
  end
end
