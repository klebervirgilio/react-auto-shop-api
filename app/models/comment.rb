class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :repair
  scope :recent, -> { order('created_at desc') }
end
